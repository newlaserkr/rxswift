//
//  PhotoRouter.swift
//  RxSwiftPortfolio
//
//  Created by Andres Felipe Alzate Restrepo on 27/07/2020.
//  Copyright © 2020 Andres Felipe Alzate Restrepo. All rights reserved.
//

import UIKit

class PhotoRouter {
    func showAlert(_ message: String, handler: ((UIAlertAction) -> Void)?) {
        let vc = UIAlertController(title: "", message: message, preferredStyle: .alert)
        vc.addAction(UIAlertAction(title: "Continuar", style: .default, handler: handler))
        UIApplication.serviceLocator.mainRouter.present(vc: vc)
    }
    
    func goBack() {
        UIApplication.serviceLocator.mainRouter.pop()
    }
    
    class func create() -> PhotoVC {
        let repository = PhotoRepositoryImpl()
        let router = PhotoRouter()
        let interactor = PhotoImpl(repository: repository)
        let presenter = PhotoPresenterImpl(router: router, interactor: interactor)
        repository.output = interactor
        interactor.output = presenter
        let vc = PhotoVC(presenter: presenter)
        presenter.output = vc
        
        return vc
    }
}
