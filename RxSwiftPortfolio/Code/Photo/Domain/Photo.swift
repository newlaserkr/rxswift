//
//  Photo.swift
//  RxSwiftPortfolio
//
//  Created by Andres Felipe Alzate Restrepo on 27/07/2020.
//  Copyright © 2020 Andres Felipe Alzate Restrepo. All rights reserved.
//

import Foundation
import Photos

protocol PhotoOutput: class {
    func getPhotosSucces(photos: PHFetchResult<PHAsset>)
    func getPhotosFailure(_ messageError: String)
}

protocol Photo {
    func getPhotos()
}

class PhotoImpl: Photo {
    private let repository: PhotoRepository
    
    weak var output: PhotoOutput?
    
    init(repository: PhotoRepository) {
        self.repository = repository
    }
    
    func getPhotos() {
        requestPermissionsIfNeeded()
    }
    
    func requestPermissionsIfNeeded() {
        PHPhotoLibrary.requestAuthorization({ [weak self] status in
            DispatchQueue.main.async {
                switch status {
                case .authorized:
                    self?.repository.getPhotos()
                case .denied:
                    self?.output?.getPhotosFailure("photo_authorization_status_denied".localized())
                case .restricted:
                    self?.output?.getPhotosFailure("photo_authorization_status_restricted".localized())
                default:
                    self?.output?.getPhotosFailure("photo_authorization_status_unknow")
                }
            }
        })
    }
}
// MARK: - PhotoRepositoryOutput
extension PhotoImpl: PhotoRepositoryOutput {
    func getPhotosSucces(photos: PHFetchResult<PHAsset>) {
        output?.getPhotosSucces(photos: photos)
    }
}
