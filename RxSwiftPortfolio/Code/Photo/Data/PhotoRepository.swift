//
//  PhotoRepository.swift
//  RxSwiftPortfolio
//
//  Created by Andres Felipe Alzate Restrepo on 27/07/2020.
//  Copyright © 2020 Andres Felipe Alzate Restrepo. All rights reserved.
//

import Foundation
import Photos

protocol PhotoRepositoryOutput: class {
    func getPhotosSucces(photos: PHFetchResult<PHAsset>)
}

protocol PhotoRepository {
    func getPhotos()
}

class PhotoRepositoryImpl: PhotoRepository {
    weak var output: PhotoRepositoryOutput?
    
    func getPhotos() {
        let allPhotosOptions = PHFetchOptions()
        allPhotosOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
        let photos = PHAsset.fetchAssets(with: allPhotosOptions)
        output?.getPhotosSucces(photos: photos)
    }
}
