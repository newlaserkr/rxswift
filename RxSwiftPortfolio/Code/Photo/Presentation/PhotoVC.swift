//
//  PhotoVC.swift
//  RxSwiftPortfolio
//
//  Created by Andres Felipe Alzate Restrepo on 27/07/2020.
//  Copyright © 2020 Andres Felipe Alzate Restrepo. All rights reserved.
//

import UIKit
import Photos

class PhotoVC: UIViewController {
    private let presenter: PhotoPresenter
    
    private var photos: PHFetchResult<PHAsset>?
    
    init(presenter: PhotoPresenter) {
        self.presenter = presenter
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.getPhotos()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    //MARK: - Setups
    private func setupView() {
        view.backgroundColor = .white
    }
}
// MARK: - PhotoPresenterOutput
extension PhotoVC: PhotoPresenterOutput {
    func getPhotosSucces(photos: PHFetchResult<PHAsset>) {
        self.photos = photos
    }
    
    func getPhotosFailure(_ messageError: String) {
        presenter.showAlert(messageError, handler: { _ in
            self.presenter.goBack()
        })
    }
}
