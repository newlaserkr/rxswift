//
//  PhotoPresenter.swift
//  RxSwiftPortfolio
//
//  Created by Andres Felipe Alzate Restrepo on 27/07/2020.
//  Copyright © 2020 Andres Felipe Alzate Restrepo. All rights reserved.
//

import UIKit
import Photos

protocol PhotoPresenterOutput: class {
    func getPhotosSucces(photos: PHFetchResult<PHAsset>)
    func getPhotosFailure(_ messageError: String)
}

protocol PhotoPresenter {
    func getPhotos()
    func showAlert(_ message: String, handler: ((UIAlertAction) -> Void)?)
    func goBack()
}

class PhotoPresenterImpl: PhotoPresenter {
    private let router: PhotoRouter
    private let interactor: Photo
    
    weak var output: PhotoPresenterOutput?
    
    init(router: PhotoRouter, interactor: Photo) {
        self.interactor = interactor
        self.router = router
    }
    
    func getPhotos() {
        interactor.getPhotos()
    }
    
    func showAlert(_ message: String, handler: ((UIAlertAction) -> Void)?) {
        router.showAlert(message, handler: handler)
    }
    
    func goBack() {
        router.goBack()
    }
}
// MARK: - PhotoOutput
extension PhotoPresenterImpl: PhotoOutput {
    func getPhotosSucces(photos: PHFetchResult<PHAsset>) {
        output?.getPhotosSucces(photos: photos)
    }
    
    func getPhotosFailure(_ messageError: String) {
        output?.getPhotosFailure(messageError)
    }
}
