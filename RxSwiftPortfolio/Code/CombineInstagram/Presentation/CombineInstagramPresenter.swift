//
//  CombineInstagramPresenter.swift
//  RxSwiftPortfolio
//
//  Created by Andres Felipe Alzate Restrepo on 27/07/2020.
//  Copyright © 2020 Andres Felipe Alzate Restrepo. All rights reserved.
//

import Foundation

protocol CombineInstagramPresenterOutput: class {

}

protocol CombineInstagramPresenter {
    func goPhoto()
}

class CombineInstagramPresenterImpl: CombineInstagramPresenter {
    private let router: CombineInstagramRouter
    private let interactor: CombineInstagram
    
    weak var output: CombineInstagramPresenterOutput?
    
    init(router: CombineInstagramRouter, interactor: CombineInstagram) {
        self.interactor = interactor
        self.router = router
    }
    
    func goPhoto() {
        router.goPhoto()
    }
}

extension CombineInstagramPresenterImpl: CombineInstagramOutput {

}
