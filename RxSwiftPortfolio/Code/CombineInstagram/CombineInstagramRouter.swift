//
//  CombineInstagramRouter.swift
//  RxSwiftPortfolio
//
//  Created by Andres Felipe Alzate Restrepo on 27/07/2020.
//  Copyright © 2020 Andres Felipe Alzate Restrepo. All rights reserved.
//

import UIKit

class CombineInstagramRouter {
    func goPhoto() {
        let vc = PhotoRouter.create()
        UIApplication.serviceLocator.mainRouter.push(vc: vc)
    }
    
    class func create() -> CombineInstagramVC {
        let repository = CombineInstagramRepositoryImpl()
        let router = CombineInstagramRouter()
        let interactor = CombineInstagramImpl(repository: repository)
        let presenter = CombineInstagramPresenterImpl(router: router, interactor: interactor)
        repository.output = interactor
        interactor.output = presenter
        let vc = CombineInstagramVC(presenter: presenter)
        presenter.output = vc
        
        return vc
    }
}
